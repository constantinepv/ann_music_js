import { INTERVAL, Interval } from './packages/interval/index';
import { CHORD, Chord } from './packages/chord/index';
import { Scale } from '@packages/scale';

console.log(Chord('C'));
console.log(Chord('Am'));
console.log(Interval(3));
console.log(Scale('major'));
